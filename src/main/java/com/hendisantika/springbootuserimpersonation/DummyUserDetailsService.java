package com.hendisantika.springbootuserimpersonation;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-user-impersonation
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-04
 * Time: 19:12
 */
@Service
public class DummyUserDetailsService implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        if ("admin".equals(username)) {
            return new User(username, encoder.encode("pass"), AuthorityUtils.createAuthorityList("ROLE_USER", "ROLE_ADMIN"));
        } else if ("user".equals(username)) {
            return new User(username, encoder.encode("pass"), AuthorityUtils.createAuthorityList("ROLE_USER"));
        }

        throw new UsernameNotFoundException("Username not found: " + username);
    }
}