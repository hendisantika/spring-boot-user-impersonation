package com.hendisantika.springbootuserimpersonation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootUserImpersonationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootUserImpersonationApplication.class, args);
    }

}

